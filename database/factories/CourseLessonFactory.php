<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CourseLessonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $test = null;
        $testTypes = ['radio', 'checkbox'];

        if (rand(0, 2)) {
            $test = [
                'questions' => [],
            ];
            for ($i = 0; $i < rand(2, 8); $i++) {
                $type = $this->faker->randomElement($testTypes);
                $answers = [];
                $countA = rand(3, 6);
                $correctSet = false;

                $correctAnswers = [];

                for ($a = 1; $a <= $countA; $a++) {
                    $correct = false;
                    if (!$correctSet) {
                        if ($a == $countA) {
                            $correct = true;
                            $correctSet = true;
                        }
                        else {
                            $correct = (bool)rand(0, 1);
                            if ($correct && $type == 'radio') {
                                $correctSet = true;
                            }
                        }
                    }
                    $answers[] = [
                        'number' => $a,
                        'text' => substr($this->faker->sentence(rand(1, 5)), 0, -1).($correct ? '*' : ''),
                    ];

                    if ($correct) {
                        $correctAnswers[] = $a;
                    }
                }
                $test['questions'][] = [
                    'type' => $type,
                    'text' => substr($this->faker->sentence(rand(4, 9)), 0, -1) . '?',
                    'answers' => $answers,
                    'correct' => $correctAnswers,
                ];
            }
        }

        return [
            'name' => substr($this->faker->sentence(rand(2, 4)), 0, -1),
            'content' => [
                'text' => implode("\r\n", $this->faker->paragraphs(rand(4, 10)))
            ],
            'test' => $test,
        ];
    }
}
