<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TeacherFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sex = rand(0, 1) ? 'male' : 'female';
 
        return [
            'login' => $this->faker->userName,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'last_name' => $this->faker->lastName($sex),
            'first_name' => $this->faker->firstName($sex),
            'middle_name' => rand(0, 4) ? $this->faker->firstName('male').($sex == 'male' ? 'ович' : 'овна') : null,
            'dob' => $this->faker->date('Y-m-d', '2000-01-01'),
            'avatar' => '',
            'position' => implode(', ', $this->faker->randomElements(['Инструктор', 'Техник', 'Электрик'], rand(1, 3)))
        ];
    }
}
