<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTeacherCourseLessonBlocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_teacher_course_lesson_blocks', function (Blueprint $table) {
            $table->foreignId('teacher_id')->contrained();
            $table->foreignId('course_lesson_block_id')->contrained();

            $table->primary(['teacher_id', 'course_lesson_block_id'], 'table_teacher_course_lesson_blocks_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_teacher_course_lesson_blocks');
    }
}
