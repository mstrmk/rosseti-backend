<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseLessonBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_lesson_blocks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('course_id')->contrained();
            $table->unsignedInteger('number');
            $table->string('name');
            $table->foreignId('course_lesson_block_type_id')->contrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_lesson_blocks');
    }
}
