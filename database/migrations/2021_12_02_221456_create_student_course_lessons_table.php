<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentCourseLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_course_lessons', function (Blueprint $table) {
            $table->foreignId('student_id')->contrained();
            $table->foreignId('course_lesson_id')->contrained();
            $table->boolean('is_read')->default(false);
            $table->boolean('is_complete')->default(false);
            $table->foreignId('teacher_id')->nullable()->contrained();

            $table->primary(['student_id', 'course_lesson_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_course_lessons');
    }
}
