<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Student;

class StudentSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (Student::count() == 0) {
            Student::factory(20)->create();
        }
    }
}
