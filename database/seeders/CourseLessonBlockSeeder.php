<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Course;
use App\Models\CourseLessonBlockType;
use App\Models\CourseLessonBlock;

class CourseLessonBlockSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (CourseLessonBlock::count() == 0) {
            $course = Course::first();
            $num = 1;
            foreach (CourseLessonBlockType::all() as $k => $type) {
                CourseLessonBlock::factory(5 - $k)->create([
                    'course_id' => $course->id,
                    'course_lesson_block_type_id' => $type->id,
                    'number' => 0,
                ])->each(function ($block) use (&$num) {
                    $block->number = $num++;
                    $block->save();
                });
            }
        }
    }
}
