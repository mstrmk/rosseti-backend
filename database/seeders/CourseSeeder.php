<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Course;
use App\Models\CourseLessonBlockType;
use App\Models\CourseLessonBlock;

class CourseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (Course::count() == 0) {
            $course = Course::create([
                'name' => 'Учебная программа по полетам на дронах',
                'is_active' => true,
            ]);
        }
    }
}
