<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Course;
use App\Models\CourseLesson;
use App\Models\CourseLessonBlock;

class CourseLessonSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (CourseLesson::count() == 0) {
            $course = Course::first();
            foreach (CourseLessonBlock::all() as $k => $block) {
                $num = 1;
                CourseLesson::factory(rand(2, 6))->create([
                    'course_lesson_block_id' => $block->id,
                    'number' => 0,
                ])->each(function ($lesson) use (&$num) {
                    $lesson->number = $num++;
                    if ($lesson->block->type->code != 'theory') {
                        $lesson->test = null;
                    }
                    $lesson->save();
                });
            }
        }
    }
}
