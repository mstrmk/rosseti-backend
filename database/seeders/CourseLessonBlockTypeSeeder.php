<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\CourseLessonBlockType;

class CourseLessonBlockTypeSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (CourseLessonBlockType::count() == 0) {
            foreach ([
                'theory' => 'Теория',
                'emulator' => 'Эмулятор',
                'practice' => 'Практика'
            ] as $code => $name) {
                CourseLessonBlockType::create([
                    'name' => $name,
                    'code' => $code,
                ]);
            }
        }
    }
}
