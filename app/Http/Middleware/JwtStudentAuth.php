<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

use ApiResponse;

use App\Helpers\Auth\StudentAuth;

class JwtStudentAuth extends BaseMiddleware
{

    public function handle($request, Closure $next)
    {
        try {
            StudentAuth::checkOrFail();
            $request->setUserResolver(function () {
                return StudentAuth::getInstance();
            });
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return ApiResponse::unauthorized('Token is invalid');
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return ApiResponse::unauthorized('Token is expired');
            } else {
                return ApiResponse::unauthorized('Authorization token not found');
            }
        }
        return $next($request);
    }
}