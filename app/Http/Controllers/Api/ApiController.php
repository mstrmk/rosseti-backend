<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ApiController extends Controller
{

    public $api = null;

    public function __construct() {
        $this->api = \Api::getInstance();
    }

}
