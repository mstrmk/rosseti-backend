<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Str;
use Carbon\Carbon;

use App\Models\Student;
use App\Models\StudentSession;

use App\Http\Resources\StudentResource;

use Api;
use ApiResponse;

class StudentAuthController extends ApiController
{

    private $auth = null;

    public function __construct()
    {
        parent::__construct();
        $this->auth = auth('student');
    }
    

    public function login(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'login' => 'required|string',
            'password' => 'required|string|min:6',
            'fingerprint' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->api->response->invalidate($validator->errors());
        }

        if (!$token = $this->auth->claims(['mode' => 'student'])->attempt([
            'login' => $request->input('login'),
            'password' => $request->input('password')
        ])) {
            return $this->api->response->unauthorized('Неверный логин или пароль');
        }

        return $this->responseAuth($token);
    }

    public function me(Request $request)
    {
        return $this->api->response->ok(new StudentResource($request->user()->get()));
    }

    public function logout(Request $request)
    {
        $this->auth->logout();
        StudentSession::where('student_id', $request->user()->id)->where('refresh_token', $request->input('refreshToken'))->delete();
        return $this->api->response->ok(['message' => 'Successfully logged out']);
    }

    public function refresh(Request $request) {
        $validator = \Validator::make($request->all(), [
            'refreshToken' => 'required|string',
            'fingerprint' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->api->response->invalidate($validator->errors());
        }

        $session = StudentSession::where('refresh_token', $request->input('refreshToken'))->first();

        if (!$session || $session->expired_at < now() || $session->fingerprint != $request->input('fingerprint')) {
            if ($session) {
                $session->delete();
            }
            return $this->api->response->unauthorized();
        }
        
        $user = $session->user;
        $token = $this->auth->login($user);

        return $this->responseAuth($token);
    }

    private function generateRefreshToken() {
        $user = $this->auth->user();
        $token = $user->id.'__REFRESH_TOKEN__'.time().'__'.Str::random(60);
        return hash('sha256', $token);
    }

    private function responseAuth($token) {
        $user = $this->auth->user();
        $request = request();
        $refreshToken = $this->generateRefreshToken();
        $refreshTokenTtl = config('jwt.refresh_ttl') * 60;

        StudentSession::create([
            'student_id' => $user->id,
            'refresh_token' => $refreshToken,
            'ip' => $request->ip(),
            'fingerprint' => $request->input('fingerprint'),
            'useragent' => $request->useragent(),
            'expired_at' => Carbon::now()->addMinutes($refreshTokenTtl),
        ]);

        return $this->respondWithTokens($token, $refreshToken);
    }

    private function respondWithTokens($token, $refreshToken) {
        return $this->api->response->ok([
            'accessToken' => $token,
            'tokenType' => 'bearer',
            'expiresIn' => $this->auth->factory()->getTTL() * 60,
            'refreshToken' => $refreshToken,
            'user' => new StudentResource($this->auth->user()),
        ]);
    }
}
