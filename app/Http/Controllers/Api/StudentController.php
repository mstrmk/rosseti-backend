<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\Student;
use App\Models\StudentCourse;

use App\Models\Course;
use App\Models\CourseLessonBlock;
use App\Models\CourseLesson;

use App\Models\StudentCourseLesson;

class StudentController extends ApiController
{

    public function courseProgress(Request $request, $id) {
        $course = Course::findOrFail($id);
        $studentId = $request->user()->id;
        $studentCourse = StudentCourse::firstOrCreate([
            'course_id' => $course->id,
            'student_id' => $studentId,
        ],
        [
            'status' => 0,
        ]);

        $studentLessons = StudentCourseLesson::where('student_id', $studentId)->with('lesson', 'lesson.block')->get(); // #VELOSIPED добавить проверку на курс
        $progress = [];
        foreach ($studentLessons as $studentLesson) {
            $progress[] = [
                'block' => $studentLesson->lesson->block->number,
                'lesson' => $studentLesson->lesson->number,
                'isRead' => $studentLesson->is_read,
                'isComplete' => $studentLesson->is_complete,
            ];
        }

        return $this->api->response->resource($progress);
    }

    public function setViewLesson(Request $request, $id, $blockNum, $lessonNum) {
        $course = Course::findOrFail($id);
        $block = CourseLessonBlock::where('course_id', $course->id)->where('number', $blockNum)->first();
        if (!$block) {
            abort(404);
        }   
        $lesson = CourseLesson::where('course_lesson_block_id', $block->id)->where('number', $lessonNum)->first();
        if (!$lesson) {
            abort(404);
        }

        $studentLesson = StudentCourseLesson::firstOrCreate([
            'student_id' => $request->user()->id,
            'course_lesson_id' => $lesson->id,
        ]);

        StudentCourseLesson::where('student_id', $request->user()->id)->where('course_lesson_id', $lesson->id)->update([
            'is_read' => true,
        ]);

        return $this->api->response->ok();
    }

    public function checkTestLesson(Request $request, $id, $blockNum, $lessonNum) {
        $course = Course::findOrFail($id);
        $block = CourseLessonBlock::where('course_id', $course->id)->where('number', $blockNum)->first();
        if (!$block) {
            abort(404);
        }   
        $lesson = CourseLesson::where('course_lesson_block_id', $block->id)->where('number', $lessonNum)->first();
        if (!$lesson) {
            abort(404);
        }

        $answers = $request->input('answers');

        $questions = $lesson->test['questions'];
        foreach ($questions as $qk => $question) {
            if (!isset($answers[$qk])) {
                return $this->api->response->ok(false);
            }
            
            $answer = $answers[$qk];
            if ($question['type'] == 'radio') {
                if ($answer != $question['correct'][0]) {
                    return $this->api->response->ok(false);
                }
            }
            else if ($question['type'] == 'checkbox') {
                if (!is_array($answer) || count($question['correct']) != count($answer)) {
                    return $this->api->response->ok(false);
                }
                foreach ($question['correct'] as $correctAns) {
                    if (!in_array($correctAns, $answer)) {
                        return $this->api->response->ok(false);
                    }
                }
            }
        }

        $studentLesson = StudentCourseLesson::firstOrCreate([
            'student_id' => $request->user()->id,
            'course_lesson_id' => $lesson->id,
        ]);

        StudentCourseLesson::where('student_id', $request->user()->id)->where('course_lesson_id', $lesson->id)->update([
            'is_read' => true,
            'is_complete' => true,
        ]);

        return $this->api->response->ok(true);
    }

    public function completeLesson(Request $request, $id, $blockNum, $lessonNum) {
        $course = Course::findOrFail($id);
        $block = CourseLessonBlock::where('course_id', $course->id)->where('number', $blockNum)->first();
        if (!$block) {
            abort(404);
        }   
        $lesson = CourseLesson::where('course_lesson_block_id', $block->id)->where('number', $lessonNum)->first();
        if (!$lesson) {
            abort(404);
        }

        $studentLesson = StudentCourseLesson::firstOrCreate([
            'student_id' => $request->user()->id,
            'course_lesson_id' => $lesson->id,
        ]);

        StudentCourseLesson::where('student_id', $request->user()->id)->where('course_lesson_id', $lesson->id)->update([
            'is_read' => true,
            'is_complete' => true,
        ]);

        return $this->api->response->ok();
    }

}
