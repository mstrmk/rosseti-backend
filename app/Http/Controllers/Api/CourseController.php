<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\Course;
use App\Models\CourseLessonBlock;
use App\Models\CourseLesson;
use App\Http\Resources\CourseResource;
use App\Http\Resources\CourseFullResource;
use App\Http\Resources\CourseLessonFullResource;

class CourseController extends ApiController
{

    public function index(Request $request) {
        return $this->api->response->resource(CourseResource::collection(Course::all()));
    }

    public function show(Request $request, $id) {
        $course = Course::findOrFail($id);
        return $this->api->response->resource(new CourseFullResource($course));
    }

    public function lesson(Request $request, $id, $blockNum, $lessonNum) {
        $course = Course::findOrFail($id);
        $block = CourseLessonBlock::where('course_id', $course->id)->where('number', $blockNum)->first();
        if (!$block) {
            abort(404);
        }   
        $lesson = CourseLesson::where('course_lesson_block_id', $block->id)->where('number', $lessonNum)->first();
        if (!$lesson) {
            abort(404);
        }

        return $this->api->response->resource(new CourseLessonFullResource($lesson));
    }

}
