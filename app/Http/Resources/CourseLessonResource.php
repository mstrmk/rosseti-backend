<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseLessonResource extends JsonResource
{
    public function toArray($request)
    {
        // $test = $this->test;

        // if ($test) {
        //     foreach ($test['questions'] as $qk => $q) {
        //         unset($test['questions'][$qk]['correct']);
        //     }
        // }

        return [
            'id' => $this->id,
            'number' => $this->number,
            'name' => $this->name,
            'withTest' => (bool)$this->test
            // 'content' => $this->content,
            // 'test' => $test,
        ];
    }
}
