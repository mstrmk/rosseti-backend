<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\CourseLesson;
use App\Models\CourseLessonBlock;

class CourseLessonFullResource extends JsonResource
{
    public function toArray($request)
    {
        $test = $this->test;

        if ($test) {
            foreach ($test['questions'] as $qk => $q) {
                unset($test['questions'][$qk]['correct']);
            }
        }

        $prevLesson = null;
        $nextLesson = null;

        if ($this->number != 1 || $this->block->number != 1) {
            if ($this->number == 1) {
                $prevBlock = CourseLessonBlock::ofNumber($this->block->number - 1)->where('course_id', $this->block->course_id)->first();
                if ($prevBlock) {
                    $prevLesson = CourseLesson::ofBlock($prevBlock->id)->orderBy('number', 'desc')->first();
                }
            }
            else {
                $prevLesson = CourseLesson::ofBlock($this->block->id)->ofNumber($this->number - 1)->first();
            }
        }

        $nextLesson = CourseLesson::ofBlock($this->block->id)->ofNumber($this->number + 1)->first();
        if (!$nextLesson) {
            $nextBlock = CourseLessonBlock::ofNumber($this->block->number + 1)->where('course_id', $this->block->course_id)->first();
            if ($nextBlock) {
                $nextLesson = CourseLesson::ofBlock($nextBlock->id)->ofNumber(1)->first();
            }
        }

        return [
            'id' => $this->id,
            'number' => $this->number,
            'name' => $this->name,
            'content' => $this->content,
            'test' => $test,
            'prev' => $prevLesson ? [
                'block' => $prevLesson->block->number,
                'lesson' => $prevLesson->number,
            ] : null,
            'next' => $nextLesson ? [
                'block' => $nextLesson->block->number,
                'lesson' => $nextLesson->number,
            ] : null,
        ];
    }
}
