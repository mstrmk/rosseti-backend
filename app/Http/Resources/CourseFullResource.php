<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseFullResource extends JsonResource
{
    public function toArray($request)
    {
        $blocks = $this->blocks;

        // if ($request->user() && $request->user()->is('student')) {
        //     $blocks = $this->blocks->filter(function($block, $key) {
        //         return $block->type->code == 'theory';
        //     });
        // }
        // else {
        //     $blocks = $this->blocks;
        // }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'blocks' => CourseLessonBlockResource::collection($blocks),
        ];
    }
}
