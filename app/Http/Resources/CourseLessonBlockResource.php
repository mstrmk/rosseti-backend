<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseLessonBlockResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'number' => $this->number,
            'type' => [
                'code' => $this->type->code,
            ],
            'lessons' => CourseLessonResource::collection($this->lessons),
        ];
    }
}
