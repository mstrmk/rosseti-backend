<?php

namespace App\Helpers\Singletone;

trait SingletoneStaticShare
{
    public static function __callStatic($method, $parameters) {
        $self = static::getInstance();
        if (method_exists($self, $method)) {
            return call_user_func_array([$self, $method], $parameters);
        }

        throw new BadMethodCallException("Method [$method] does not exist.");
    }
}