<?php

namespace App\Helpers\Singletone;

trait Singletone {

    private static $instance = null;

    private function __construct() {}
    private function __clone() {}

    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new static();
            self::$instance->boot();
        }
        return self::$instance;
    }

    public function boot() {
        //
    }
}