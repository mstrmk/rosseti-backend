<?php 

namespace App\Helpers\Api;

use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;

use App\Helpers\Singletone\Singletone;

class ApiResponse
{

    use Singletone;

    protected function _response($code, $data = null, $message = null) {
        $responseData = [];
        if (!is_null($data)) {
            $responseData['data'] = $data;
        }
        if (!is_null($message)) {
            $responseData['message'] = $message;
        }

        return $responseData ? response()->json($responseData, $code) : response()->noContent($code);
    }

    protected function _ok($data = null, $message = null) {
        return $this->_response(Response::HTTP_OK, $data, $message);
    }
    
    // GET
    // ok
    protected function _resource($data = null, $message = null) {
        return $this->_response(Response::HTTP_OK, $data, $message);
    }

    protected function _resourcePaginate($query, $page = 1, $perPage = 10, $resource = null, $message = null) {
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $result = $query->paginate($perPage);
        return $this->_response(Response::HTTP_OK, [
            'meta' => [
                'total' => (int)$result->total(),
                'from' => (int)$result->firstItem(),
                'to' => (int)$result->lastItem(),
                'countPages' => (int)$result->lastPage(),
                'page' => (int)$result->currentPage(),
                'perPage' => (int)$result->perPage(),
            ],
            'items' => $resource ? $resource::collection($result->items()) : $result->items(),
        ], $message);
    }

    // POST
    // resource
    protected function _created($data = null) {
        return $this->_response(Response::HTTP_CREATED, $data);
    }

    // PUT/PATCH
    //ok
    protected function _updated($data = null) {
        return $this->_response(Response::HTTP_OK, $data, 'updated');
    }

    // DELETE 
    // ok
    protected function _deleted() {
        return $this->_response(Response::HTTP_OK, null, 'deleted');
    }

    // ALL 
    // not found
    protected function _notExists() {
        return $this->_response(Response::HTTP_NO_CONTENT, null, null);
    }

    // POST PUT/PATCH 
    // error
    protected function _badRequest($data = null, $message = 'bad request') {
        return $this->_response(Response::HTTP_BAD_REQUEST, $data, $message);
    }

    // ALL
    // Not auth
    protected function _unauthorized($data = null, $message = null) {
        return $this->_response(Response::HTTP_UNAUTHORIZED, $data, $message ? $message : 'unauthorized');
    }

    // ALL
    // Not access
    protected function _forbidden($data = null) {
        return $this->_response(Response::HTTP_FORBIDDEN, $data, 'forbidden');
    }

    // ALL
    // Unprocessable Entity
    protected function _invalidate($errors = null) {
        return $this->_response(Response::HTTP_UNPROCESSABLE_ENTITY, $errors, 'not validated');
    }

    protected function _serverError() {
        return $this->_response(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public static function __callStatic($method, $parameters) {
        $self = new static();

        if (method_exists($self, '_'.$method)) {
            return call_user_func_array([$self, '_'.$method], $parameters);
        }

        throw new BadMethodCallException("Method [$method] does not exist.");
    }

    public function __call($method, $parameters) {
        $self = new static();

        if (method_exists($self, '_'.$method)) {
            return call_user_func_array([$self, '_'.$method], $parameters);
        }

        throw new BadMethodCallException("Method [$method] does not exist.");
    }

}