<?php 

namespace App\Helpers\Api;

use App\Helpers\Singletone\Singletone;

use ApiResponse;

class Api
{

    use Singletone;

    public $response = null;

    public function boot() {
        $this->response = ApiResponse::getInstance();
    }

    protected function _try($callback) {
        try {
            return $callback();
        }
        catch (\Exception $e) {
            if (!config('app.debug')) {
                return ApiResponse::serverError(); 
            }

            throw $e;
        }
    }

    protected function _tryTransaction($callback) {
        return $this->_try(function() use ($callback) {
            $result = null;
            \DB::transaction(function () use ($callback, &$result) {
                $result = $callback();
            });
            return $result;
        });
    }

    protected function _withValidate($rules, $callback) {
        $validator = \Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return ApiResponse::invalidate($validator->errors());
        }

        $data = $validator->validated();

        return $callback($data);
    }

    public static function __callStatic($method, $parameters) {
        $self = new static();

        if (method_exists($self, '_'.$method)) {
            return call_user_func_array([$self, '_'.$method], $parameters);
        }

        throw new BadMethodCallException("Method [$method] does not exist.");
    }

    public function __call($method, $parameters) {
        $self = new static();

        if (method_exists($self, '_'.$method)) {
            return call_user_func_array([$self, '_'.$method], $parameters);
        }

        throw new BadMethodCallException("Method [$method] does not exist.");
    }

}