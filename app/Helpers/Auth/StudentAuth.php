<?php

namespace App\Helpers\Auth;

use App\Helpers\Singletone\{
    Singletone,
    SingletoneStaticShare
};

use App\Models\Student;

use JWTAuth;
use Exception;

class StudentAuth
{

    use Singletone, SingletoneStaticShare;

    private $user = null;
    public $id = null;
    public $mode = 'student';

    private function checkOrFail() {
        $payload = JWTAuth::parseToken()->checkOrFail();
        $mode = $payload->get('mode');
        if ($mode != 'student') {
            throw new \Tymon\JWTAuth\Exceptions\TokenInvalidException;
        }

        $this->id = $payload->get('sub');
        return $this->id;
    }

    private function check() {
        try {
            $this->checkOrFail();
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    public function is($mode) {
        return $mode === $this->mode;
    }

    public function get() {
        if (is_null($this->user)) {
            if (!$this->id) {
                throw new Exception('Auth user id not defined');
            }
            $this->user = Student::find($this->id);
            if (!$this->user) {
                auth('student')->logout();
            }
        }
        return $this->user;
    }

}