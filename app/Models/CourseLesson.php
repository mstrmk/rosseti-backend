<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseLesson extends Model
{
    use HasFactory;

    public $casts = [
        'content' => 'array',
        'test' => 'array',
    ];

    public function block() {
        return $this->belongsTo(CourseLessonBlock::class, 'course_lesson_block_id', 'id');
    }

    public function scopeOfBlock($query, $block) {
        return $query->where('course_lesson_block_id', $block);
    }

    public function scopeOfNumber($query, $block) {
        return $query->where('number', $block);
    }
}
