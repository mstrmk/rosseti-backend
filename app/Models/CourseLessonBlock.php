<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseLessonBlock extends Model
{
    use HasFactory;

    public function lessons() {
        return $this->hasMany(CourseLesson::class)->orderBy('number');
    }

    public function type() {
        return $this->belongsTo(CourseLessonBlockType::class, 'course_lesson_block_type_id', 'id');
    }

    public function scopeOfNumber($query, $block) {
        return $query->where('number', $block);
    }
}
