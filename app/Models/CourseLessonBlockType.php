<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseLessonBlockType extends Model
{
    use HasFactory;

    public function scopeOfCode($query, $code) {
        return $query->where('code', $code);
    }
}
