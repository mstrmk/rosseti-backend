<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentCourseLesson extends Model
{
    use HasFactory;

    protected $guarded = [];
    public $timestamps = false;

    public function lesson() {
        return $this->belongsTo(CourseLesson::class, 'course_lesson_id', 'id');
    }
}
