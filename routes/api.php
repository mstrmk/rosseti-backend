<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth/student',
], 
function ($router) {
    Route::post('login', [Api\Auth\StudentAuthController::class, 'login']);
    Route::post('refresh', [Api\Auth\StudentAuthController::class, 'refresh']);

    Route::group([
        'middleware' => 'student.auth'
    ], 
    function ($router) {
        Route::post('logout', [Api\Auth\StudentAuthController::class, 'logout']);
        Route::get('me', [Api\Auth\StudentAuthController::class, 'me']);
    });
});

Route::group([
    'prefix' => 'student',
    'middleware' => 'student.auth'
], 
function ($router) {
    Route::get('/courses', [Api\CourseController::class, 'index']);
    Route::get('/courses/{id}', [Api\CourseController::class, 'show']);
    Route::get('/courses/{id}/progress', [Api\StudentController::class, 'courseProgress']);
    Route::get('/courses/{id}/{blockNum}/{lessonNum}', [Api\CourseController::class, 'lesson']);
    Route::post('/courses/{id}/{blockNum}/{lessonNum}/view', [Api\StudentController::class, 'setViewLesson']);
    Route::post('/courses/{id}/{blockNum}/{lessonNum}/complete', [Api\StudentController::class, 'completeLesson']);
    Route::post('/courses/{id}/{blockNum}/{lessonNum}/test', [Api\StudentController::class, 'checkTestLesson']);
});
